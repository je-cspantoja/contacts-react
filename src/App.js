import React from 'react';
import {ContactsView} from './views/';
import {ContactsViewPresenter} from './presenters/';
import {ContactsDataSource} from './datasources/';

export default function App() {
  const contactsPresenter = new ContactsViewPresenter(
    new ContactsDataSource()
  );

  return (
    <ContactsView presenter={contactsPresenter}/>
  );
}
