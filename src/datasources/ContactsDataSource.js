import uuid4 from 'uuid/v4';
import {Contact} from '../models';

export default class ContactsDataSource {

  constructor() {
    this.contactsStore = {};
  }

  create({name, phone}) {

    const newContact = new Contact({
      id: uuid4(),
      name,
      phone
    });

    this.contactsStore[newContact.getId()] = newContact;
    return newContact;
  }

  remove(contactId) {
    const deleteContact = this.contactsStore[contactId];
    delete this.contactsStore[contactId];
    return deleteContact;
  }

  list() {
    const contacts = Object.values(
      this.contactsStore
    );

    return contacts;
  }
}
