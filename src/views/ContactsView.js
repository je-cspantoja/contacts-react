import React, {Component} from 'react';
import { AddContact, ContactList } from '../components';

export default class Contacts extends Component {

  constructor(props) {
    super(props);

    const {presenter} = props;
    this.presenter = presenter;

    this.state = {
      name: '',
      phone: '',
      contacts: [],
    };

    this.presenter.setView(this);
  }

  componentDidMount() {
    this.presenter.init();
  }

  setContacts(contacts) {
    this.setState({contacts});
  }

  setName(name) {
    this.setState({name});
  }

  setPhone(phone) {
    this.setState({phone});
  }

  onAddContact() {
    const {name, phone} = this.state;
    this.presenter.onAddContact({name, phone});
  }

  onRemoveContact(contactId) {
    this.presenter.onRemoveContact(contactId);
  }

  clearAddContactComponent() {
    this.setName('');
    this.setPhone('');
  }

  render(){
    const {name, phone, contacts} = this.state;

    return (
      <>
        <center>
          <h1>
            Contacts Page
          </h1>
        </center>

        <AddContact
          name={name}
          phone={phone}
          onChangeName={this.setName.bind(this)}
          onChangePhone={this.setPhone.bind(this)}
          onAddContact={this.onAddContact.bind(this)} />

        <br />

        <ContactList
          contacts={contacts}
          onRemoveContact={this.onRemoveContact.bind(this)} />
      </>
    );
  }
}
