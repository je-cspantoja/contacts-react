export default class ContactsPresenter {
  constructor(contactsDataSource) {
    this.contactsDataSource = contactsDataSource;
  }

  init() {
    this.updateViewContacts();
  }

  setView(contactsView){
    this.view = contactsView;
  }

  onAddContact({name, phone}) {
    this.contactsDataSource.create({name, phone});
    this.view.clearAddContactComponent();
    this.updateViewContacts();
  }

  onRemoveContact(contactId) {
    this.contactsDataSource.remove(contactId);
    this.updateViewContacts();
  }

  updateViewContacts() {
    this.view.setContacts(this.contactsDataSource.list());
  }
}
