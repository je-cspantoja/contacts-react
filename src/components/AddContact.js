import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';


const useStyles = makeStyles(theme => ({
  paper: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
  },
  nameInputText: {
    marginLeft: theme.spacing(1),
    flex: 2,
  },
  phoneInputText: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  addButton: {
    flex: 1,
    padding: 10,
  },
}));

export default function AddContact(props) {
    const {
      name,
      phone,
      onChangeName,
      onChangePhone,
      onAddContact,
    } = props;

    const classes = useStyles();
    const onChangeNameAdapter = ({ target }) => onChangeName(target.value);
    const onChangePhoneAdapter = ({ target }) => onChangePhone(target.value);

    return (
      <Paper component="form" className={classes.paper} elevation={3}>
        <TextField
          className={classes.nameInputText}
          value={name}
          label="name"
          onChange={onChangeNameAdapter}/>

        <TextField
          className={classes.phoneInputText}
          value={phone}
          label="phone"
          onChange={onChangePhoneAdapter} />

        <Divider className={classes.divider} orientation="vertical" />

        <Button
          className={classes.addButton}
          onClick={onAddContact}
          variant="contained"
          color="primary" >
            Add
        </Button>

      </Paper>
    );
}