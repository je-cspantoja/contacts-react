import AddContact from './AddContact';
import {ContactList} from './contactList/';

export {
  AddContact,
  ContactList,
};