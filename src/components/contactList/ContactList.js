import React from 'react';

import {List, ListItem} from '../List';
import {Contact} from './contact/';


export default function ContactList({contacts, onRemoveContact}) {

  const renderListItems = (contact, index) => (
    <ListItem key={index}>
      <Contact
        contact={contact}
        onRemoveContact={onRemoveContact} />
    </ListItem>
  );

  return (
    <List>
      {contacts.map(renderListItems)}
    </List>
  );

}
