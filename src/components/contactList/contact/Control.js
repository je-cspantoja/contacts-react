import React from 'react';
import Button from '@material-ui/core/Button';

export default function Control({onRemoveContact}) {
    return (
      <>
        <Button onClick={onRemoveContact} variant="contained" color="primary">
          Remove
        </Button>
      </>
    );
  }