import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

import Detail from './Detail'
import Control from './Control'


const useStyles = makeStyles(theme => ({
  paper: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    margin: 2,
  },

  divider: {
    height: 28,
    margin: 4,
  },
}));


export default function Contact({contact, onRemoveContact}) {
    const classes = useStyles();

    const onRemoveContactAdapter = () => {
      onRemoveContact(contact.getId());
    }

    return (
      <Paper className={classes.paper} elevation={3}>
        <Detail contact={contact} />
        <Divider className={classes.divider} orientation="vertical" />
        <Control onRemoveContact={onRemoveContactAdapter}/>
      </Paper>
    );
  }

