import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  nameInputText: {
    marginLeft: theme.spacing(1),
    flex: 2,
  },
  phoneInputText: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
}));

export default function Detail({contact}) {
  const classes = useStyles();

    return (
      <>
        <TextField
          disabled
          label="name"
          className={classes.nameInputText}
          value={contact.getName()} />

        <TextField
         disabled
         label="phone"
         className={classes.phoneInputText}
          value={contact.getPhone()} />
      </>
    );
  }
