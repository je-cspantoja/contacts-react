import React from 'react';

export function List({children}) {
  return (
    <div>
      {children}
    </div>
  );
}

export function ListItem({children}) {
  return (
    <div>
      {children}
    </div>
  );
}