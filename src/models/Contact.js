export default class Contact {

  constructor({id, name, phone}) {
      this._id = id;
      this._name = name;
      this._phone = phone;
  }

  getId() {
    return this._id;
  }

  getName() {
    return this._name;
  }

  getPhone() {
    return this._phone;
  }

}